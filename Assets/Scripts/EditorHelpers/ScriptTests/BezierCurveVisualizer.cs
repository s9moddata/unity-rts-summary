﻿#if UNITY_EDITOR

namespace Assets.EditorHelpers.ScriptTests
{
    using System.Linq;
    using Assets.Scripts.BezierCurves;
    using Assets.Scripts.Extensions;
    using UnityEngine;

    [ExecuteInEditMode]
    public class BezierCurveVisualizer : MonoBehaviour
    {
        [Header("Geometry")]
        [SerializeField]
        private Transform _point0;

        [SerializeField]
        private Transform _point1;

        [SerializeField]
        private Transform _point2;

        [Header("Graphics")]
        [SerializeField]
        private LineRenderer _lineRenderer;

        [Header("Settings")]
        [SerializeField]
        private int _curvePoints = 10;

        [SerializeField]
        private bool _includeP0 = true;

        [SerializeField]
        private float _radius = 3f;

        private void Update()
        {
            if (_point0 == null || _point1 == null || _point2 == null || _lineRenderer == null)
            {
                return;
            }

            DrawCurve();
        }

        private void DrawCurve()
        {
            Vector3 p0 = Vector3Extensions.LerpByDistance(_point1.position, _point0.position, _radius);
            Vector3 p2 = Vector3Extensions.LerpByDistance(_point1.position, _point2.position, _radius);

            Vector3[] points = BezierCurve.CalculateQuadraticCurve(p0, _point1.position, p2, _curvePoints, _includeP0).ToArray();

            _lineRenderer.positionCount = _includeP0 ? _curvePoints + 1 : _curvePoints;
            _lineRenderer.SetPositions(points);
        }
    }
}

#endif

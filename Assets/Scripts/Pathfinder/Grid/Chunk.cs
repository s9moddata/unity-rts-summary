﻿namespace Assets.Scripts.Pathfinder.Grid
{
    using System;
    using System.Collections.Generic;
    using Assets.Scripts.CustomTypes.Serializable;
    using Assets.Scripts.Extensions;
    using UnityEngine;
    using UnityRandom = UnityEngine.Random;

    [Serializable]
    public class Chunk
    {
        [SerializeField]
        private SerializableHashSet<Cell> _chuckCells;

        [SerializeField]
        private int _chunkSizeX;

        [SerializeField]
        private int _chunkSizeY;

        [SerializeField]
        private Vector2Int _chunkBorderMin;

        [SerializeField]
        private Vector2Int _chunkBorderMax;

        [SerializeField]
        private Color _chunkDebugColor;

        public Chunk(Cell[,] chuckCells, int chunkSizeX, int chunkSizeY, Vector2Int chunkBorderMin, Vector2Int chunkBorderMax)
        {
            const float hueMin = 0.55f;
            const float hueMax = 0.75f;
            const float saturationMin = 0.5f;
            const float saturationMax = 1f;
            const float valueMin = 0.6f;
            const float valueMax = 1f;
            const float alphaMin = 0.5f;
            const float alphaMax = 0.5f;

            _chunkSizeX = chunkSizeX;
            _chunkSizeY = chunkSizeY;
            _chunkBorderMin = chunkBorderMin;
            _chunkBorderMax = chunkBorderMax;
            _chuckCells = chuckCells.ToSerializableHashSet(_chunkSizeX, _chunkSizeY);
            _chunkDebugColor = UnityRandom.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, valueMin, valueMax, alphaMin, alphaMax);
        }

        public HashSet<Cell> ChuckCells => _chuckCells.HashSet;

        public Color ChunkDebugColor => _chunkDebugColor;

        public bool ContainsCell(Vector2Int cellPosition)
        {
            bool border1 = _chunkBorderMin.x <= cellPosition.x && _chunkBorderMin.y <= cellPosition.y;
            bool border2 = _chunkBorderMax.x >= cellPosition.x && _chunkBorderMax.y >= cellPosition.y;
            return border1 && border2;
        }

        public void GenerateClearance()
        {
            const int minRange = 1;
            int chunkMaxSize = Math.Max(_chunkSizeX, _chunkSizeY);
            Cell[,] chuckCells = ChuckCells.ToArray2D(_chunkSizeX, _chunkSizeY);

            for (int x = 0; x < _chunkSizeX; x++)
            {
                for (int y = 0; y < _chunkSizeY; y++)
                {
                    if (chuckCells[x, y].IsObstacle)
                    {
                        continue;
                    }

                    for (int range = minRange; range < chunkMaxSize; range++)
                    {
                        chuckCells[x, y] = Cell.Copy(chuckCells[x, y], range);

                        if (IsObstacleInRange(chuckCells, range, chuckCells[x, y].ChunkIndexX, chuckCells[x, y].ChunkIndexY))
                        {
                            break;
                        }
                    }
                }
            }

            _chuckCells = chuckCells.ToSerializableHashSet(_chunkSizeX, _chunkSizeY);
        }

        private bool IsObstacleInRange(Cell[,] chuckCells, int range, int cellX, int cellY)
        {
            for (int coordinate = 0; coordinate <= range; coordinate++)
            {
                bool cell1Inside = coordinate + cellX < _chunkSizeX && range + cellY < _chunkSizeY;
                bool cell2Inside = coordinate + cellY < _chunkSizeY && range + cellX < _chunkSizeX;

                bool isObstacleCell1 = cell1Inside && chuckCells[coordinate + cellX, range + cellY].IsObstacle;
                bool isObstacleCell2 = cell2Inside && chuckCells[range + cellX, coordinate + cellY].IsObstacle;

                if (isObstacleCell1 || isObstacleCell2)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

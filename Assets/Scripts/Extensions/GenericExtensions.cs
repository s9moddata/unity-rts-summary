﻿namespace Assets.Scripts.Extensions
{
    public static class GenericExtensions
    {
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
    }
}

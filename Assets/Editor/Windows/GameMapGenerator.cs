﻿namespace Assets.Editor.Windows
{
    using System.Collections.Generic;
    using System.IO;
    using Assets.Scripts.Pathfinder.Grid;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEditor;
    using UnityEngine;

    public class GameMapGenerator : EditorWindow
    {
        private const int GridSizeMultiplier = 10;
        private const string MainFolderName = "GameMapTerrain";
        private const string TerrainFolderName = "Terrains";
        private const string ScriptableObjectFolderName = "ScriptableObjects";

        private int _gridSizeX = 0;
        private int _gridSizeY = 0;

        private string _mapName = string.Empty;

        private bool _instantiateMap = true;
        private bool _isSolidMap = true;

        private GameMapSolid _gameMapSolid;

        [MenuItem("WW2/Game Map/Generate")]
        private static void CreateWindow()
        {
            EditorWindow window = GetWindow(typeof(GameMapGenerator));
            window.titleContent = new GUIContent("Generate Game Map");
            window.minSize = new Vector2(300f, 400f);
            window.position = new Rect(150f, 100f, 300f, 400f);
        }

        private void OnGUI()
        {
            const int gridSizeMin = 2;
            const int gridSizeMax = 48;

            _isSolidMap = EditorGUILayout.Toggle("Make map solid", _isSolidMap);
            _mapName = EditorGUILayout.TextField("File Name", _mapName);

            if (_isSolidMap)
            {
                _gridSizeX = EditorGUILayout.IntSlider("Grid Size X", _gridSizeX, gridSizeMin, gridSizeMax);
                _gridSizeY = EditorGUILayout.IntSlider("Grid Size Y", _gridSizeY, gridSizeMin, gridSizeMax);
            }
            else
            {
                _gameMapSolid = EditorGUILayout.ObjectField("Solid map data", _gameMapSolid, typeof(GameMapSolid), true) as GameMapSolid;
            }

            _instantiateMap = EditorGUILayout.Toggle("Instantiate map", _instantiateMap);

            EditorGUILayout.Space();

            if (GUILayout.Button("Create"))
            {
                ValidateDataFolderPaths();
                CreateTerrain();
                CreateScriptableObject();
            }
        }

        private void CreateTerrain()
        {
            const float gridHeight = 0f;
            const float arrayBound = 1f;

            float gridSizeX = _gridSizeX * GridSizeMultiplier - arrayBound;
            float gridSizeY = _gridSizeY * GridSizeMultiplier - arrayBound;

            string fileName = FileName;

            TerrainData terrainData = new TerrainData();
            terrainData.name = fileName;
            terrainData.size = new Vector3(gridSizeX, gridHeight, gridSizeY);

            if (_instantiateMap)
            {
                GameObject terrain = Terrain.CreateTerrainGameObject(terrainData);
                terrain.name = fileName;
            }

            AssetDatabase.CreateAsset(terrainData, $"Assets/{MainFolderName}/{TerrainFolderName}/{fileName}.asset");
        }

        private void CreateScriptableObject()
        {
            Object gameMap;
            Grid2D grid2D = new Grid2D();

            int gridSizeX = _gridSizeX * GridSizeMultiplier;
            int gridSizeY = _gridSizeY * GridSizeMultiplier;

            string fileName = $"Assets/{MainFolderName}/{ScriptableObjectFolderName}/{FileName}";

            if (_isSolidMap)
            {
                fileName = string.Concat(fileName, "_SOLID");

                Cell[,] cells = grid2D.GenerateCells(gridSizeX, gridSizeY);
                gameMap = GameMapSolid.CreateInstance(cells, gridSizeX, gridSizeY);
            }
            else
            {
                List<Chunk> chunks = grid2D.GenerateChunks(_gameMapSolid);
                gameMap = GameMap.CreateInstance(chunks, _gameMapSolid.GridSizeX, _gameMapSolid.GridSizeY);
            }

            AssetDatabase.CreateAsset(gameMap, $"{fileName}.asset");
        }

        //private void CreatePrefab()
        //{
        //    GameObject mainObject = new GameObject(FileName);
        //    GameObject pathfinderObject = new GameObject(nameof(PathfinderManager), typeof(PathfinderManager));
        //    pathfinderObject.transform.parent = mainObject.transform;

        //    GameObject prefab = PrefabUtility.SaveAsPrefabAsset(mainObject, $"Assets/{MainFolderName}/{FileName}.prefab");

        //    DestroyImmediate(mainObject);
        //}

        private string FileName
        {
            get
            {
                string mapName = string.IsNullOrWhiteSpace(_mapName) ? "MAP" : _mapName;
                return $"{mapName}_{_gridSizeX}X_{_gridSizeY}Y";
            }
        }

        private void ValidateDataFolderPaths()
        {
            string pathToCheck1 = $"{Application.dataPath}/{MainFolderName}/{ScriptableObjectFolderName}";
            string pathToCheck2 = $"{Application.dataPath}/{MainFolderName}/{TerrainFolderName}";

            if (!Directory.Exists(pathToCheck1))
            {
                Directory.CreateDirectory(pathToCheck1);
            }

            if (!Directory.Exists(pathToCheck2))
            {
                Directory.CreateDirectory(pathToCheck2);
            }
        }
    }
}

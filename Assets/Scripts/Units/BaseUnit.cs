﻿namespace Assets.Scripts.Units
{
    using System.Collections.Generic;
    using Assets.Scripts.Units.ScriptableObjects;
    using UnityEngine;

    [RequireComponent(typeof(MovementLogic))]
    internal class BaseUnit : BaseMonobehaviourWithId
    {
        [Header("Data")]
        [SerializeField]
        private BaseUnitData _unitData;

        [Header("Selection Logic")]
        [SerializeField]
        private GameObject _selectionMarker;

        [SerializeField]
        private Collider _boundingBoxCollider;

        [Header("Movement Logic")]
        [SerializeField]
        private MovementLogic _movementLogic;

        public Vector3 Position => transform.position;

        public Collider BoundingBoxCollider => _boundingBoxCollider;

        public int Clearance => _unitData.Clearance;

        public void SetSelectionMarketActive(bool isActive) => _selectionMarker.SetActive(isActive);

        public void FollowPath(IReadOnlyList<Vector2> path) => _movementLogic.Setup(path, _unitData.RotationSpeed, _unitData.MovementSpeed);

        private void StopFollowingPath() => _movementLogic.Stop();
    }
}

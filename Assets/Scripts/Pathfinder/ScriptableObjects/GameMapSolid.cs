﻿namespace Assets.Scripts.Pathfinder.ScriptableObjects
{
    using System;
    using Assets.Scripts.CustomTypes.Serializable;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.Constants;
    using Assets.Scripts.Pathfinder.Grid;
    using UnityEngine;

    [Serializable]
    public class GameMapSolid : ScriptableObject
    {
        [SerializeField]
        private int _gridSizeX;

        [SerializeField]
        private int _gridSizeY;

        [SerializeField]
        [HideInInspector]
        private Serializable2DArray<Cell> _gridCells;

        public static GameMapSolid CreateInstance(Cell[,] gridCells, int gridLengthX, int gridLengthY)
        {
            GameMapSolid data = CreateInstance<GameMapSolid>();
            data._gridCells = new Serializable2DArray<Cell>(gridCells, gridLengthX, gridLengthY);
            data._gridSizeX = gridLengthX;
            data._gridSizeY = gridLengthY;

            return data;
        }

        public Cell[,] Cells => _gridCells.Array;

        public int GridSizeX => _gridSizeX;

        public int GridSizeY => _gridSizeY;

        public void SetObstacleStatus(Vector2Int cellPosition, bool isObstacle)
        {
            if (!ArrayExtensions.IndexExistsIn2DArray(cellPosition.x, cellPosition.y, GridSizeX, GridSizeY))
            {
                return;
            }

            Cells[cellPosition.x, cellPosition.y] = Cell.Copy(Cells[cellPosition.x, cellPosition.y], isObstacle);
        }

        public void GenerateClearance()
        {
            const int minRange = 1;
            int maxRange = Math.Max(GridSizeX, GridSizeY);
            maxRange = Math.Min(maxRange, MapGrid.ChunkSize);

            for (int x = 0; x < GridSizeX; x++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (Cells[x, y].IsObstacle)
                    {
                        continue;
                    }

                    for (int range = minRange; range < maxRange; range++)
                    {
                        if (IsObstacleInRange(range, Cells[x, y].WorldPosition.x, Cells[x, y].WorldPosition.y))
                        {
                            Cells[x, y] = Cell.Copy(Cells[x, y], range);
                            break;
                        }
                    }
                }
            }
        }

        private bool IsObstacleInRange(int range, int cellX, int cellY)
        {
            // Check square corner cells
            bool rightTop = IsCellObstacle(cellX + range, cellY + range);
            bool leftTop = IsCellObstacle(cellX - range, cellY + range);
            bool rightBot = IsCellObstacle(cellX + range, cellY - range);
            bool leftBot = IsCellObstacle(cellX - range, cellY - range);

            if (rightTop || leftTop || rightBot || leftBot)
            {
                return true;
            }

            for (int offset = 0; offset < range; offset++)
            {
                // Check cells on square range, except corners cells
                bool top1 = IsCellObstacle(cellX + offset, cellY + range);
                bool top2 = IsCellObstacle(cellX - offset, cellY + range);
                bool bot1 = IsCellObstacle(cellX + offset, cellY - range);
                bool bot2 = IsCellObstacle(cellX - offset, cellY - range);
                bool left1 = IsCellObstacle(cellX - range, cellY + offset);
                bool left2 = IsCellObstacle(cellX - range, cellY - offset);
                bool right1 = IsCellObstacle(cellX + range, cellY + offset);
                bool right2 = IsCellObstacle(cellX + range, cellY - offset);

                if (top1 || top2 || bot1 || bot2 || left1 || left2 || right1 || right2)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsCellObstacle(int cellX, int cellY)
            => !ArrayExtensions.IndexExistsIn2DArray(cellX, cellY, GridSizeX, GridSizeY) || Cells[cellX, cellY].IsObstacle;
    }
}

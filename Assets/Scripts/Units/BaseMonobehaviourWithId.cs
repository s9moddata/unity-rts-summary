﻿namespace Assets.Scripts.Units
{
    using System;
    using Assets.Scripts.CustomTypes.Serializable;
    using UnityEngine;

    public abstract class BaseMonobehaviourWithId : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField]
        private SerializableGuid _guid;

        private void Awake()
        {
            _guid = new SerializableGuid(Guid.NewGuid());
        }

        public SerializableGuid ObjectGuid => _guid;
    }
}

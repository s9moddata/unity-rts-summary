﻿namespace Assets.Scripts.Units
{
    using System.Collections.Generic;
    using Assets.Scripts.BezierCurves;
    using Assets.Scripts.CustomTypes;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.Constants;
    using UnityEngine;

    public class MovementLogic : MonoBehaviour
    {
        [Header("Geometry")]
        [SerializeField]
        private Transform _unitTransform;

        private float _rotationSpeed;
        private float _movementSpeed;

        private ReadOnlyIndexedList<Vector2> _pathfinderPath;
        private ReadOnlyIndexedList<Vector2> _currentPath;

        private Vector3 _movePoint;

        public void Setup(IReadOnlyList<Vector2> pathfinderPath, float rotationSpeed, float movementSpeed)
        {
            if (pathfinderPath.IsNullOrEmpty())
            {
                Stop();
                return;
            }

            _rotationSpeed = rotationSpeed;
            _movementSpeed = movementSpeed;

            _pathfinderPath = new ReadOnlyIndexedList<Vector2>(pathfinderPath);
            _currentPath = new ReadOnlyIndexedList<Vector2>(CalculatePath());

            _movePoint = CalculateMovePoint();
        }

        public void Stop()
        {
            _pathfinderPath = new ReadOnlyIndexedList<Vector2>();
            _currentPath = new ReadOnlyIndexedList<Vector2>();
        }

        private void Update()
        {
            MoveAndRotate();
        }

        private void MoveAndRotate()
        {
            if (_pathfinderPath.IsInvalid() && _currentPath.IsInvalid())
            {
                return;
            }

            Rotate(_movePoint);
            bool reachedMovePoint = Move(_movePoint);

            if (reachedMovePoint)
            {
                bool pathOutOfBounds = _currentPath.Increase();

                if (pathOutOfBounds && !_pathfinderPath.IsEmptyOrOutOfBounds)
                {
                    _currentPath = new ReadOnlyIndexedList<Vector2>(CalculatePath());
                }

                if (!_currentPath.IsEmptyOrOutOfBounds)
                {
                    _movePoint = CalculateMovePoint();
                }
            }
        }

        private bool Move(Vector3 movePoint)
        {
            float speed = _movementSpeed * Time.deltaTime;
            _unitTransform.position = Vector3.MoveTowards(_unitTransform.position, movePoint, speed);
            return _unitTransform.position == movePoint;
        }

        private void Rotate(Vector3 movePoint)
        {
            Vector3 lookRotation = movePoint - _unitTransform.position;

            if (lookRotation == Vector3.zero)
            {
                return;
            }

            float rotationSpeed = _rotationSpeed * Time.deltaTime;
            Quaternion rotateTo = Quaternion.LookRotation(movePoint - _unitTransform.position);
            _unitTransform.rotation = Quaternion.RotateTowards(_unitTransform.rotation, rotateTo, rotationSpeed);
        }

        private Vector3 CalculateMovePoint() => new Vector3(_currentPath.Value.x, _unitTransform.position.y, _currentPath.Value.y);

        private IReadOnlyList<Vector2> CalculatePath()
        {
            const int maxPositions = 3;
            const int offset1 = 1;
            const int offset2 = 2;

            List<Vector2> positions = new List<Vector2>
            {
                Vector3Extensions.ToVector2ZY(_unitTransform.position)
            };

            if (_pathfinderPath.TryGetValue(out Vector2 positionToCheck, offset1))
            {
                positions.Add(positionToCheck);
            }

            if (_pathfinderPath.TryGetValue(out positionToCheck, offset2))
            {
                positions.Add(positionToCheck);
            }

            _pathfinderPath.Increase();

            return positions.Count == maxPositions
                ? CalculateBezierCurvePath(positions)
                : positions;
        }

        private IReadOnlyList<Vector2> CalculateBezierCurvePath(IReadOnlyList<Vector2> positions)
        {
            const float maxPercent = 100f;
            const float onePercent = 1.8f;
            const int curvePointsMin = 4;
            const int curvePointsMax = 10;
            const float curveRadiusMax = 7f;

            float angle = Vector2Extensions.Angle(positions[0], positions[2], positions[1]);
            float anglePercent = maxPercent - (angle / onePercent);

            float distance0 = Vector2.Distance(positions[0], positions[1]);
            float distance2 = Vector2.Distance(positions[2], positions[1]);
            float curveRadius = Mathf.Min(distance0, distance2);
            curveRadius = Mathf.Clamp(curveRadius, MapGrid.HalfCellWidht, curveRadiusMax);

            int curvePoints = NumericExtensions.GetValueOnAxisByPercent(curvePointsMin, curvePointsMax, anglePercent);

            return BezierCurve.CalculateQuadraticCurve(positions[0], positions[1], positions[2], curvePoints, curveRadius);
        }
    }
}

﻿namespace Assets.Scripts
{
    using System.Collections.Generic;
    using System.Linq;
    using Assets.Scripts.BezierCurves;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder;
    using Assets.Scripts.Units;
    using UnityEngine;

    public class Unit : MonoBehaviour
    {
        [SerializeField]
        private MovementLogic _movementLogic;

        public Camera _playerCamera = null;
        public Collider _terrainCollider = null;
        public Terrain _terrain = null;
        public Transform _pointerTransform = null;
        public float _rotationSpeed = 10f;
        public float _speed = 10f;
        public int _clearance = 1;

        private Vector3 _movePoint = Vector3.zero;
        private IReadOnlyList<Vector2> _path;
        private int currentPoint = 0;

        private void Update()
        {
            PlayerInput();
        }

        private void PlayerInput()
        {
            if (Input.GetMouseButtonDown(1))
            {
                Ray ray = _playerCamera.ScreenPointToRay(Input.mousePosition);
                bool wasHit = _terrainCollider.Raycast(ray, out RaycastHit hit, Mathf.Infinity);

                if (wasHit)
                {
                    IReadOnlyList<Vector2> path = PathfinderManager.GetPath(_pointerTransform.position, hit.point, _clearance);

                    _movementLogic.Setup(path, _rotationSpeed, _speed);
                }
            }
        }
    }
}

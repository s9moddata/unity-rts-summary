﻿namespace Assets.Scripts
{
    using System.Collections.Generic;
    using Assets.Scripts.Extensions;
    using UnityEngine;

    public class GameMapObject : MonoBehaviour
    {
        [Header("Geometry")]
        [SerializeField]
        private Transform _objectTransform = null;

        [SerializeField]
        private MeshCollider _objectCollider = null;

        [SerializeField]
        private BoundingBox _boundingBox = null;

        public IReadOnlyCollection<Vector2Int> GetMeshVertices()
        {
            List<Vector2Int> meshGridVertices = new List<Vector2Int>();

            Vector2Int objectPosition = _objectTransform.position.RoundToVector2IntZY();

            foreach (Vector2Int vertex in _boundingBox.ObjectGridVertices)
            {
                Vector2Int vertexGridPosition = objectPosition + vertex;

                Vector3 worldVertexPosition = new Vector3(vertexGridPosition.x, 0f, vertexGridPosition.y);
                worldVertexPosition = worldVertexPosition.RotateAroundPivot(_objectTransform.position, _objectTransform.rotation);

                int worldVertexPositionX = Mathf.RoundToInt(worldVertexPosition.x);
                int worldVertexPositionZ = Mathf.RoundToInt(worldVertexPosition.z);

                Vector3 worldVertexPositionRounded = new Vector3(worldVertexPositionX, _objectCollider.bounds.center.y, worldVertexPositionZ);

                bool isInsideBounds = _objectCollider.bounds.Contains(worldVertexPositionRounded);

                if (isInsideBounds)
                {
                    meshGridVertices.Add(new Vector2Int(worldVertexPositionX, worldVertexPositionZ));
                }
            }

            return meshGridVertices;
        }

        public List<Vector2Int> GetInitialMeshVertices()
        {
            float maxX = float.MinValue;
            float minX = float.MaxValue;
            float maxZ = float.MinValue;
            float minZ = float.MaxValue;

            foreach (Vector3 vertex in _objectCollider.sharedMesh.vertices)
            {
                Vector3 scaledVertex = Vector3.Scale(vertex, _objectTransform.localScale);

                if (scaledVertex.x > maxX)
                {
                    maxX = scaledVertex.x;
                }

                if (scaledVertex.x < minX)
                {
                    minX = scaledVertex.x;
                }

                if (scaledVertex.z > maxZ)
                {
                    maxZ = scaledVertex.z;
                }

                if (scaledVertex.z < minZ)
                {
                    minZ = scaledVertex.z;
                }
            }

            int minX_I = Mathf.RoundToInt(minX);
            int maxX_I = Mathf.RoundToInt(maxX);
            int minZ_I = Mathf.RoundToInt(minZ);
            int maxZ_I = Mathf.RoundToInt(maxZ);

            List<Vector2Int> meshGridVertices = new List<Vector2Int>();

            for (int x = minX_I; x <= maxX_I; x++)
            {
                for (int z = minZ_I; z <= maxZ_I; z++)
                {
                    meshGridVertices.Add(new Vector2Int(x, z));
                }
            }

            return meshGridVertices;
        }
    }
}

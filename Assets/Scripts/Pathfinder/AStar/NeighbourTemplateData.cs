﻿namespace Assets.Scripts.Pathfinder.AStar
{
    using UnityEngine;

    public struct NeighbourTemplateData
    {
        public NeighbourTemplateData(Vector2Int position, float cost)
        {
            Position = position;
            Cost = cost;
        }

        public Vector2Int Position { get; }

        public float Cost { get; }
    }
}

﻿namespace Assets.Scripts.Pathfinder.AStar
{
    using System;
    using UnityEngine;

    public readonly struct PathNode : IComparable<PathNode>
    {
        private const float MinimalTraversedDistance = 0f;
        private readonly float EstimatedTotalCost;

        public PathNode(Vector2Int position, Vector2Int neighbourOf, Vector2Int target, float traversedDistance = MinimalTraversedDistance)
        {
            Position = position;
            NeighbourOf = neighbourOf;
            TraversedDistance = traversedDistance;
            EstimatedTotalCost = traversedDistance + Vector2Int.Distance(position, target);
            IsPathStartPosition = position == neighbourOf;
        }

        public Vector2Int Position { get; }

        public Vector2Int NeighbourOf { get; }

        public float TraversedDistance { get; }

        public bool IsPathStartPosition { get; }

        public int CompareTo(PathNode other) => EstimatedTotalCost.CompareTo(other.EstimatedTotalCost);
    }
}

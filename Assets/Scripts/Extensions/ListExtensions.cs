﻿namespace Assets.Scripts.Extensions
{
    using System.Collections.Generic;

    public static class ListExtensions
    {
        public static T[,] ToArray2D<T>(this List<T> list, int arrayLengthX, int arrayLengthY)
        {
            T[,] array2d = new T[arrayLengthX, arrayLengthY];

            for (int row = 0; row < arrayLengthX; row++)
            {
                for (int column = 0; column < arrayLengthY; column++)
                {
                    int index = row * arrayLengthY + column;
                    array2d[row, column] = list[index];
                }
            }

            return array2d;
        }
    }
}

﻿namespace Assets.Scripts.Pathfinder.AStar
{
    using System;
    using System.Collections.Generic;
    using Assets.Scripts.BinaryHeap;
    using Assets.Scripts.CustomTypes;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.Constants;
    using Assets.Scripts.Pathfinder.Grid;
    using UnityEngine;

    public class PathfinderAStar
    {
        private readonly NeighbourTemplateData[] NeighboursTemplate = {
            new NeighbourTemplateData(new Vector2Int(1, 0), MapGrid.CellWidht),
            new NeighbourTemplateData(new Vector2Int(0, 1), MapGrid.CellWidht),
            new NeighbourTemplateData(new Vector2Int(-1, 0), MapGrid.CellWidht),
            new NeighbourTemplateData(new Vector2Int(0, -1), MapGrid.CellWidht),
            new NeighbourTemplateData(new Vector2Int(1, 1), MapGrid.CellDiagonalWidht),
            new NeighbourTemplateData(new Vector2Int(1, -1), MapGrid.CellDiagonalWidht),
            new NeighbourTemplateData(new Vector2Int(-1, 1), MapGrid.CellDiagonalWidht),
            new NeighbourTemplateData(new Vector2Int(-1, -1), MapGrid.CellDiagonalWidht)
        };

        private readonly ReadOnlyArray2D<Cell> Cells;
        private readonly HashSet<PathNode> Neighbours;
        private readonly BinaryHeap<Vector2Int, PathNode> PositionsToAnalyze;
        private readonly HashSet<Vector2Int> IgnoredPositions;
        private readonly List<Vector2> Output;
        private readonly IDictionary<Vector2Int, Vector2Int> Links;

        public PathfinderAStar(ReadOnlyArray2D<Cell> cells, int initialCapacity = 0)
        {
            const int maxNeighbours = 8;

            Cells = cells;
            Neighbours = new HashSet<PathNode>(maxNeighbours);
            PositionsToAnalyze = new BinaryHeap<Vector2Int, PathNode>(a => a.Position, initialCapacity);
            IgnoredPositions = new HashSet<Vector2Int>(initialCapacity);
            Output = new List<Vector2>(initialCapacity);
            Links = new Dictionary<Vector2Int, Vector2Int>(initialCapacity);
        }

        /// <summary>
        /// Calculates a path between two points using Lazy Theta A*.
        /// </summary>
        /// <returns>Returns <see cref="true"/> when path is found.</returns>
        public bool Calculate(Vector2Int start, Vector2Int target, int agentClearance, out IReadOnlyList<Vector2> path)
        {
            if (!Cells.ContainsIndex(start) || !Cells.ContainsIndex(target))
            {
                path = new List<Vector2>();
                return false;
            }

            bool isPathFound = GenerateNodes(start, target, agentClearance);

            if (!isPathFound)
            {
                path = new List<Vector2>();
                return false;
            }

            Output.Clear();
            Output.Add(target);

            while (Links.TryGetValue(target, out target))
            {
                Output.Add(target);
            }

            Output.Reverse();
            path = Output;
            return true;
        }

        /// <returns>Returns <see cref="true"/> if path is found.</returns>
        private bool GenerateNodes(Vector2Int start, Vector2Int target, int agentClearance)
        {
            PositionsToAnalyze.Clear();
            IgnoredPositions.Clear();
            Links.Clear();

            PositionsToAnalyze.Enqueue(new PathNode(start, start, target));

            while (PositionsToAnalyze.Count > 0)
            {
                PathNode current = PositionsToAnalyze.Dequeue();
                IgnoredPositions.Add(current.Position);

                if (current.Position == target)
                {
                    return true;
                }

                UpdateNeighboursLazy(current, target, agentClearance);
            }

            return false;
        }

        private void UpdateNeighboursLazy(PathNode center, Vector2Int target, int agentClearance)
        {
            bool hasLineOfSight =
               Links.TryGetValue(center.Position, out Vector2Int centerParentPosition) &&
               HasLineOfSight(center.Position, centerParentPosition, agentClearance);

            Vector2Int parentPosition = hasLineOfSight ? centerParentPosition : center.NeighbourOf;

            if (!hasLineOfSight && !center.IsPathStartPosition)
            {
                Links[center.Position] = center.NeighbourOf;
            }

            UpdateNeighbours(center, target, agentClearance);

            foreach (PathNode newNode in Neighbours)
            {
                if (IgnoredPositions.Contains(newNode.Position))
                {
                    continue;
                }

                AnalyzeNode(newNode, parentPosition);
            }
        }

        private void AnalyzeNode(PathNode pathNode, Vector2Int parentPosition)
        {
            if (!PositionsToAnalyze.TryGet(pathNode.Position, out PathNode existingNode))
            {
                PositionsToAnalyze.Enqueue(pathNode);
                Links[pathNode.Position] = parentPosition;
            }
            else if (pathNode.TraversedDistance < existingNode.TraversedDistance)
            {
                PositionsToAnalyze.Modify(pathNode);
                Links[pathNode.Position] = parentPosition;
            }
        }

        private void UpdateNeighbours(PathNode parent, Vector2Int target, int agentClearance)
        {
            Neighbours.Clear();

            foreach (NeighbourTemplateData neighbour in NeighboursTemplate)
            {
                // NeighbourOf value of the path is just it's 'center'. But this value should be found
                // among all neighbour of the node that are in ignored list:
                // http://vodacek.zvb.cz/archiv/stranky/850fbbd474dc2ca0023e773ae24aaa97.png

                Vector2Int nodePosition = neighbour.Position + parent.Position;

                if (Cells.TryGetValue(nodePosition, out Cell cell) && !cell.IsObstacle && cell.Clearance >= agentClearance)
                {
                    PathNode newNode = new PathNode(nodePosition, parent.Position, target, parent.TraversedDistance + neighbour.Cost);
                    Neighbours.Add(newNode);
                }
            }
        }

        private bool HasLineOfSight(Vector2Int start, Vector2Int end, int agentClearance)
        {
            int x0 = start.x;
            int y0 = start.y;
            int x1 = end.x;
            int y1 = end.y;

            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);

            if (steep)
            {
                GenericExtensions.Swap(ref x0, ref y0);
                GenericExtensions.Swap(ref x1, ref y1);
            }

            if (x0 > x1)
            {
                GenericExtensions.Swap(ref x0, ref x1);
                GenericExtensions.Swap(ref y0, ref y1);
            }

            int dX = x1 - x0;
            int dY = Math.Abs(y1 - y0);

            int err = dX / 2;
            int ystep = y0 < y1 ? 1 : -1;
            int y = y0;

            for (int x = x0; x <= x1; ++x)
            {
                Vector2Int positionToCheck = steep ? new Vector2Int(y, x) : new Vector2Int(x, y);

                if (Cells.TryGetValue(positionToCheck, out Cell cell) && (cell.IsObstacle || cell.Clearance < agentClearance))
                {
                    return false;
                }

                err -= dY;

                if (err < 0)
                {
                    y += ystep;
                    err += dX;
                }
            }

            return true;
        }
    }
}

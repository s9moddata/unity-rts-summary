﻿namespace Assets.Scripts.Extensions
{
    using UnityEngine;

    public static class NumericExtensions
    {
        private const float MaxPercent = 100f;

        public static int GetValueOnAxisByPercent(int minAxisValue, int maxAxisValue, float percent)
        {
            float onePercent = (maxAxisValue - minAxisValue) / MaxPercent;
            float valueOnAxis = minAxisValue + (onePercent * percent);
            return Mathf.RoundToInt(valueOnAxis);
        }

        public static float GetValueOnAxisByPercent(float minAxisValue, float maxAxisValue, float percent)
        {
            float onePercent = (maxAxisValue - minAxisValue) / MaxPercent;
            return minAxisValue + (onePercent * percent);
        }

        public static bool InRange(this float value, float minRange, float maxRange) => minRange <= value && value <= maxRange;
    }
}

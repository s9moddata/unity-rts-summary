﻿namespace Assets.Scripts.CustomTypes.Serializable
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class SerializableHashSet<T> : ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<T> _serializableList = null;

        private HashSet<T> _hashSet = null;

        public SerializableHashSet()
        {
            _hashSet = new HashSet<T>();
        }

        public HashSet<T> HashSet => _hashSet;

        public void OnBeforeSerialize()
        {
            _serializableList = new List<T>(_hashSet);
        }

        public void OnAfterDeserialize()
        {
            _hashSet = new HashSet<T>();

            foreach (T item in _serializableList)
            {
                _hashSet.Add(item);
            }

            _serializableList = null;
        }
    }
}

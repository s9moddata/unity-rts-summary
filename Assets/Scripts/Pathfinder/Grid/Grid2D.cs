﻿namespace Assets.Scripts.Pathfinder.Grid
{
    using System.Collections.Generic;
    using Assets.Scripts.Pathfinder.Constants;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEngine;

    public class Grid2D
    {
        public List<Chunk> GenerateChunks(int gridSizeX, int gridSizeY)
        {
            const int initialValue = 0;

            int chunkStartX = initialValue;
            int chunkStartY = initialValue;

            int cellOffsetX = initialValue;
            int cellOffsetY = initialValue;

            bool reachedEndX = false;
            bool reachedEndY = false;

            List<Chunk> chunks = new List<Chunk>();

            while (!reachedEndX || !reachedEndY)
            {
                int chunkSizeX = MapGrid.ChunkSize;
                int chunkSizeY = MapGrid.ChunkSize;

                if (chunkStartX + chunkSizeX > gridSizeX)
                {
                    chunkSizeX -= chunkStartX + chunkSizeX - gridSizeX;
                }

                if (chunkStartY + chunkSizeY > gridSizeY)
                {
                    chunkSizeY -= chunkStartY + chunkSizeY - gridSizeY;
                }

                int chunkOffsetX = cellOffsetX * MapGrid.ChunkSize;
                int chunkOffsetY = cellOffsetY * MapGrid.ChunkSize;

                Cell[,] chunkCells = GenerateCells(chunkSizeX, chunkSizeY, chunkOffsetX, chunkOffsetY);

                Vector2Int borderMin = new Vector2Int(chunkOffsetX, chunkOffsetY);
                Vector2Int borderMax = new Vector2Int(chunkOffsetX + chunkSizeX - 1, chunkOffsetY + chunkSizeY - 1);

                Chunk chunk = new Chunk(chunkCells, chunkSizeX, chunkSizeY, borderMin, borderMax);
                chunks.Add(chunk);

                reachedEndX = chunkStartX + MapGrid.ChunkSize >= gridSizeX;
                reachedEndY = chunkStartY + MapGrid.ChunkSize >= gridSizeY;

                if (reachedEndX)
                {
                    chunkStartX = initialValue;
                    chunkStartY += MapGrid.ChunkSize;

                    cellOffsetX = initialValue;
                    cellOffsetY++;
                }
                else
                {
                    chunkStartX += MapGrid.ChunkSize;

                    cellOffsetX++;
                }
            }

            return chunks;
        }

        public List<Chunk> GenerateChunks(GameMapSolid gameMapSolid)
        {
            const int initialValue = 0;

            int chunkStartX = initialValue;
            int chunkStartY = initialValue;

            int cellOffsetX = initialValue;
            int cellOffsetY = initialValue;

            bool reachedEndX = false;
            bool reachedEndY = false;

            List<Chunk> chunks = new List<Chunk>();

            while (!reachedEndX || !reachedEndY)
            {
                int chunkSizeX = MapGrid.ChunkSize;
                int chunkSizeY = MapGrid.ChunkSize;

                if (chunkStartX + chunkSizeX > gameMapSolid.GridSizeX)
                {
                    chunkSizeX -= chunkStartX + chunkSizeX - gameMapSolid.GridSizeX;
                }

                if (chunkStartY + chunkSizeY > gameMapSolid.GridSizeY)
                {
                    chunkSizeY -= chunkStartY + chunkSizeY - gameMapSolid.GridSizeY;
                }

                int chunkOffsetX = cellOffsetX * MapGrid.ChunkSize;
                int chunkOffsetY = cellOffsetY * MapGrid.ChunkSize;

                Cell[,] chunkCells = ExtractChunk(gameMapSolid.Cells, chunkSizeX, chunkSizeY, chunkOffsetX, chunkOffsetY);

                Vector2Int borderMin = new Vector2Int(chunkOffsetX, chunkOffsetY);
                Vector2Int borderMax = new Vector2Int(chunkOffsetX + chunkSizeX - 1, chunkOffsetY + chunkSizeY - 1);

                Chunk chunk = new Chunk(chunkCells, chunkSizeX, chunkSizeY, borderMin, borderMax);
                chunks.Add(chunk);

                reachedEndX = chunkStartX + MapGrid.ChunkSize >= gameMapSolid.GridSizeX;
                reachedEndY = chunkStartY + MapGrid.ChunkSize >= gameMapSolid.GridSizeY;

                if (reachedEndX)
                {
                    chunkStartX = initialValue;
                    chunkStartY += MapGrid.ChunkSize;

                    cellOffsetX = initialValue;
                    cellOffsetY++;
                }
                else
                {
                    chunkStartX += MapGrid.ChunkSize;

                    cellOffsetX++;
                }
            }

            return chunks;
        }

        public Cell[,] GenerateCells(int sizeX, int sizeY, int chunkOffsetX = 0, int chunkOffsetY = 0)
        {
            Cell[,] cells = new Cell[sizeX, sizeY];

            for (int row = 0; row < sizeX; row++)
            {
                for (int column = 0; column < sizeY; column++)
                {
                    int cellX = row + chunkOffsetX;
                    int cellY = column + chunkOffsetY;
                    Vector2Int coordinate = new Vector2Int(cellX, cellY);

                    cells[row, column] = new Cell(coordinate, row, column);
                }
            }

            return cells;
        }

        private Cell[,] ExtractChunk(Cell[,] cells, int chunkSizeX, int chunkSizeY, int chunkOffsetX, int chunkOffsetY)
        {
            Cell[,] chunk = new Cell[chunkSizeX, chunkSizeY];

            for (int row = 0; row < chunkSizeX; row++)
            {
                for (int column = 0; column < chunkSizeY; column++)
                {
                    int cellX = row + chunkOffsetX;
                    int cellY = column + chunkOffsetY;

                    chunk[row, column] = cells[cellX, cellY];
                }
            }

            return chunk;
        }
    }
}

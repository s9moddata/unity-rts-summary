﻿namespace Assets.Scripts.CustomTypes.Serializable
{
    using System;
    using System.Collections.Generic;
    using Assets.Scripts.Extensions;
    using UnityEngine;

    [Serializable]
    public class Serializable2DArray<T> : ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<T> _serializableList = null;

        [SerializeField]
        private int _arrayLengthX;

        [SerializeField]
        private int _arrayLengthY;

        private T[,] _array = null;

        public Serializable2DArray(T[,] array, int arrayLengthX, int arrayLengthY)
        {
            _array = array;
            _arrayLengthX = arrayLengthX;
            _arrayLengthY = arrayLengthY;
        }

        public T[,] Array => _array;

        public void OnBeforeSerialize()
        {
            _serializableList = _array.ToList(_arrayLengthX, _arrayLengthY);
        }
        
        public void OnAfterDeserialize()
        {
            _array = _serializableList.ToArray2D(_arrayLengthX, _arrayLengthY);
            _serializableList = null;
        }
    }
}

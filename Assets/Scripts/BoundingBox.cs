﻿namespace Assets.Scripts
{
    using System.Collections.Generic;
    using UnityEngine;

    public class BoundingBox : ScriptableObject
    {
        [SerializeField]
        private List<Vector2Int> _objectGridVertices = null;

        /// <summary>
        /// Returns object verticles with initial rotation.
        /// </summary>
        public List<Vector2Int> ObjectGridVertices => _objectGridVertices;

        public static BoundingBox CreateInstance(List<Vector2Int> objectGridVertices)
        {
            BoundingBox data = CreateInstance<BoundingBox>();
            data._objectGridVertices = objectGridVertices;

            return data;
        }
    }
}

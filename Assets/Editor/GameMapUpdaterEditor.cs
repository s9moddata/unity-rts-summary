﻿namespace Assets.Editor
{
    using System.Collections.Generic;
    using Assets.Scripts.EditorHelpers;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(GameMapUpdater))]
    public class GameMapUpdaterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }

		private void OnSceneGUI()
        {
            GameMapUpdater gameMapUpdater = target as GameMapUpdater;

            CreateUI(gameMapUpdater);

            if (gameMapUpdater._useSolidMap && gameMapUpdater.HasSolidMapData)
            {
                UpdateGridBrush(gameMapUpdater);
                UpdateCommands(gameMapUpdater);
            }
        }

        private void CreateUI(GameMapUpdater gameMapUpdater)
        {
            const int gridBrushSizeMin = 1;
            const int gridBrushSizeMax = 10;

            string showButtonText = gameMapUpdater._showWindow ? "Disable Map Updater" : "Activate Map Updater";

            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            Handles.BeginGUI();
            GUILayout.BeginArea(gameMapUpdater.WindowRect);

            if (GUILayout.Button(showButtonText))
            {
                gameMapUpdater._showWindow = !gameMapUpdater._showWindow;
            }

            if (gameMapUpdater._showWindow)
            {
                gameMapUpdater._useSolidMap = EditorGUILayout.ToggleLeft("Use solid map", gameMapUpdater._useSolidMap, EditorStyles.textField);

                if (gameMapUpdater._useSolidMap)
                {
                    GUILayout.TextArea($"Raycast to the map terrain using {gameMapUpdater.PlaceObstacleKey}");

                    GUILayout.BeginHorizontal();
                    GUILayout.TextField("Map Terrain");
                    gameMapUpdater._terrainCollider = EditorGUILayout.ObjectField(gameMapUpdater._terrainCollider, typeof(TerrainCollider), true) as TerrainCollider;
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.TextField("Solid Map Data");
                    gameMapUpdater._gameMapSolid = EditorGUILayout.ObjectField(gameMapUpdater._gameMapSolid, typeof(GameMapSolid), true) as GameMapSolid;
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.TextField("Brush size");
                    gameMapUpdater._gridBrushSize = EditorGUILayout.IntSlider(gameMapUpdater._gridBrushSize, gridBrushSizeMin, gridBrushSizeMax);
                    GUILayout.EndHorizontal();
                }
                else
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.TextField("Map Data");
                    gameMapUpdater._gameMap = EditorGUILayout.ObjectField(gameMapUpdater._gameMap, typeof(GameMap), true) as GameMap;
                    GUILayout.EndHorizontal();
                }

                gameMapUpdater._showGrid = EditorGUILayout.ToggleLeft("Show grid", gameMapUpdater._showGrid, EditorStyles.textField);
                gameMapUpdater._showClearance = EditorGUILayout.ToggleLeft("Show clearance", gameMapUpdater._showClearance, EditorStyles.textField);

                if (gameMapUpdater._useSolidMap && GUILayout.Button("Update and save map"))
                {
                    gameMapUpdater.UpdateAndSaveGameMap();
                }
            }

            GUILayout.EndArea();
            Handles.EndGUI();
        }

        private void UpdateGridBrush(GameMapUpdater gameMapUpdater)
        {
            gameMapUpdater._gridBrush = new HashSet<Vector2Int>();

            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            if (gameMapUpdater._terrainCollider.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                int offset = gameMapUpdater._gridBrushSize / 2;

                for (int xOffset = 0; xOffset < gameMapUpdater._gridBrushSize; xOffset++)
                {
                    for (int yOffset = 0; yOffset < gameMapUpdater._gridBrushSize; yOffset++)
                    {
                        int hitX = Mathf.RoundToInt(hit.point.x) + xOffset;
                        int hitY = Mathf.RoundToInt(hit.point.z) + yOffset;
                        Vector2Int position = new Vector2Int(hitX, hitY) - new Vector2Int(offset, offset);
                        gameMapUpdater._gridBrush.Add(position);
                    }
                }
            }
        }

        private void UpdateCommands(GameMapUpdater gameMapUpdater)
        {
            bool placeObstacle = Event.current.keyCode == gameMapUpdater.PlaceObstacleKey && Event.current.type == EventType.KeyDown;
            bool removeObstacle = Event.current.keyCode == gameMapUpdater.RemoveObstacleKey && Event.current.type == EventType.KeyDown;

            if (placeObstacle)
            {
                gameMapUpdater.SetObstacleStatus(gameMapUpdater._gridBrush, true);
            }

            if (removeObstacle)
            {
                gameMapUpdater.SetObstacleStatus(gameMapUpdater._gridBrush, false);
            }
        }
    }
}

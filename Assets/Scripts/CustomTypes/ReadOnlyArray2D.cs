﻿namespace Assets.Scripts.CustomTypes
{
    using Assets.Scripts.Extensions;
    using UnityEngine;

    public class ReadOnlyArray2D<T>
    {
        private readonly T[,] Array;
        private readonly int ArrayLenght0;
        private readonly int ArrayLenght1;

        public ReadOnlyArray2D(T[,] array)
        {
            Array = array;

            ArrayLenght0 = array.GetLength(0);
            ArrayLenght1 = array.GetLength(1);
        }

        public bool TryGetValue(int rowIndex, int columnIndex, out T value)
        {
            if (ContainsIndex(rowIndex, columnIndex))
            {
                value = Array[rowIndex, columnIndex];
                return true;
            }

            value = default(T);
            return false;
        }

        public bool TryGetValue(Vector2Int index, out T value) => TryGetValue(index.x, index.y, out value);

        public bool ContainsIndex(int rowIndex, int columnIndex) => ArrayExtensions.IndexExistsIn2DArray(rowIndex, columnIndex, ArrayLenght0, ArrayLenght1);

        public bool ContainsIndex(Vector2Int index) => ContainsIndex(index.x, index.y);
    }
}

﻿namespace Assets.Scripts.Extensions
{
    using System;
    using UnityEngine;

    public static class Vector2IntExtensions
    {
        /// <returns>Returns a vector with absolute coordinates.</returns>
        public static Vector2Int Abs(Vector2Int vector2Int) => new Vector2Int(Math.Abs(vector2Int.x), Math.Abs(vector2Int.y));
    }
}

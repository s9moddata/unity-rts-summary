﻿namespace Assets.Scripts.Pathfinder.ScriptableObjects
{
    using System;
    using System.Collections.Generic;
    using Assets.Scripts.Pathfinder.Grid;
    using UnityEngine;

    [Serializable]
    public class GameMap : ScriptableObject
    {
        [SerializeField]
        private int _gridSizeX;

        [SerializeField]
        private int _gridSizeY;

        [SerializeField]
        [HideInInspector]
        private List<Chunk> _chunks;

        public List<Chunk> Chunks => _chunks;

        public static GameMap CreateInstance(List<Chunk> chunks, int gridSizeX, int gridSizeY)
        {
            GameMap data = CreateInstance<GameMap>();
            data._chunks = chunks;
            data._gridSizeX = gridSizeX;
            data._gridSizeY = gridSizeY;

            return data;
        }
    }
}

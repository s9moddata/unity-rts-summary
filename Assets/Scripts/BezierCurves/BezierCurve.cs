﻿namespace Assets.Scripts.BezierCurves
{
    using System.Collections.Generic;
    using Assets.Scripts.Extensions;
    using UnityEngine;

    public static class BezierCurve
    {
        public static IReadOnlyList<Vector2> CalculateQuadraticCurve(
           Vector2 p0,
           Vector2 p1,
           Vector2 p2,
           int curvePoints,
           float radius,
           bool includeP0 = true) => CalculateQuadraticCurve(p0, p1, p2, curvePoints, radius, radius, includeP0);

        public static IReadOnlyList<Vector2> CalculateQuadraticCurve(
            Vector2 p0,
            Vector2 p1,
            Vector2 p2,
            int curvePoints,
            float radiusP0,
            float radiusP2,
            bool includeP0 = true)
        {
            Vector2 p0d = Vector2Extensions.LerpByDistance(p1, p0, radiusP0);
            Vector2 p2d = Vector2Extensions.LerpByDistance(p1, p2, radiusP2);

            return CalculateQuadraticCurve(p0d, p1, p2d, curvePoints, includeP0);
        }

        /// <param name="p0">Point 0.</param>
        /// <param name="p1">Point 1.</param>
        /// <param name="p2">Point 2.</param>
        /// <param name="curvePoints">Number of curve points to be calculated.</param>
        /// <param name="includeP0">Include the start point into the final result.</param>
        /// <returns>Returns a collection of a bezier curve points.</returns>
        public static IReadOnlyList<Vector3> CalculateQuadraticCurve(
            Vector3 p0,
            Vector3 p1,
            Vector3 p2,
            int curvePoints,
            bool includeP0 = true)
        {
            List<Vector3> points = new List<Vector3>(includeP0 ? curvePoints + 1 : curvePoints);

            if (includeP0)
            {
                points.Add(p0);
            }

            for (int i = 1; i <= curvePoints; i++)
            {
                float t = i / (float)curvePoints;
                points.Add(CalculateQuadraticPoint(t, p0, p1, p2));
            }

            return points;
        }

        /// <param name="p0">Point 0.</param>
        /// <param name="p1">Point 1.</param>
        /// <param name="p2">Point 2.</param>
        /// <param name="curvePoints">Number of curve points to be calculated.</param>
        /// <param name="includeP0">Include the start point into the final result.</param>
        /// <returns>Returns a collection of a bezier curve points.</returns>
        public static IReadOnlyList<Vector2> CalculateQuadraticCurve(
            Vector2 p0,
            Vector2 p1,
            Vector2 p2,
            int curvePoints,
            bool includeP0 = true)
        {
            List<Vector2> points = new List<Vector2>(includeP0 ? curvePoints + 1 : curvePoints);

            if (includeP0)
            {
                points.Add(p0);
            }

            for (int i = 1; i <= curvePoints; i++)
            {
                float t = i / (float)curvePoints;
                points.Add(CalculateQuadraticPoint(t, p0, p1, p2));
            }

            return points;
        }

        /// <param name="t">Function value.</param>
        /// <param name="p0">Point 0.</param>
        /// <param name="p1">Point 1.</param>
        /// <param name="p2">Point 2.</param>
        /// <returns>Returns a point on a quadratic curve.</returns>
        private static Vector3 CalculateQuadraticPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;
            Vector3 p = uu * p0;
            p += 2 * u * t * p1;
            p += tt * p2;
            return p;
        }

        /// <param name="t">Function value.</param>
        /// <param name="p0">Point 0.</param>
        /// <param name="p1">Point 1.</param>
        /// <param name="p2">Point 2.</param>
        /// <returns>Returns a point on a quadratic curve.</returns>
        private static Vector2 CalculateQuadraticPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;
            Vector2 p = uu * p0;
            p += 2 * u * t * p1;
            p += tt * p2;
            return p;
        }
    }
}

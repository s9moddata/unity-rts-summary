﻿namespace Assets.Scripts.Extensions
{
    using System.Collections.Generic;
    using Assets.Scripts.CustomTypes.Serializable;

    public static class ArrayExtensions
    {
        /// <param name="index0">Element index in 0 dimension.</param>
        /// <param name="index1">Element index in 1 dimension.</param>
        /// <param name="arrayLength0">Max number of elements in 2D array or grid in 0 dimension.</param>
        /// <param name="arrayLength1">Max number of elements in 2D array or grid in 1 dimension.</param>
        public static bool IndexExistsIn2DArray(int index0, int index1, int arrayLength0, int arrayLength1)
        {
            const int arrayIndexMin = -1;
            return index0 > arrayIndexMin && index1 > arrayIndexMin && index0 < arrayLength0 && index1 < arrayLength1;
        }

        public static List<T> ToList<T>(this T[,] array2d, int arrayLengthX, int arrayLengthY)
        {
            List<T> list = new List<T>();

            for (int row = 0; row < arrayLengthX; row++)
            {
                for (int column = 0; column < arrayLengthY; column++)
                {
                    list.Add(array2d[row, column]);
                }
            }

            return list;
        }

        public static SerializableHashSet<T> ToSerializableHashSet<T>(this T[,] array2d, int arrayLengthX, int arrayLengthY)
        {
            SerializableHashSet<T> hashSet = new SerializableHashSet<T>();

            for (int row = 0; row < arrayLengthX; row++)
            {
                for (int column = 0; column < arrayLengthY; column++)
                {
                    hashSet.HashSet.Add(array2d[row, column]);
                }
            }

            return hashSet;
        }
    }
}

﻿namespace Assets.Scripts.World
{
    using System.Collections.Generic;
    using Assets.Scripts.Units;
    using UnityEngine;

    internal class WorldUnitsList : MonoBehaviour
    {
        private Dictionary<Collider, BaseUnit> _unitByCollider;

        private void Start()
        {
            FindAllUnits();
        }

        private void FindAllUnits()
        {
            BaseUnit[] units = FindObjectsOfType<BaseUnit>();

            _unitByCollider = new Dictionary<Collider, BaseUnit>();

            foreach (BaseUnit unit in units)
            {
                _unitByCollider[unit.BoundingBoxCollider] = unit;
            }
        }

        public BaseUnit GetUnit(Collider unitCollider) => _unitByCollider[unitCollider];
    }
}

﻿namespace Assets.Scripts.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    public static class IEnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable) => enumerable == null || !enumerable.Any();

        public static T[,] ToArray2D<T>(this IEnumerable<T> enumerable, int arrayLengthX, int arrayLengthY)
        {
            T[,] array2d = new T[arrayLengthX, arrayLengthY];

            for (int row = 0; row < arrayLengthX; row++)
            {
                for (int column = 0; column < arrayLengthY; column++)
                {
                    int index = row * arrayLengthY + column;
                    array2d[row, column] = enumerable.ElementAt(index);
                }
            }

            return array2d;
        }
    }
}

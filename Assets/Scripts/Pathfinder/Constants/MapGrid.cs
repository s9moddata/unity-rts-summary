﻿namespace Assets.Scripts.Pathfinder.Constants
{
    using UnityEngine;

    public static class MapGrid
    {
        public const int ChunkSize = 15;
        public const int CellWidht = 1;
        public const float HalfCellWidht = CellWidht * 0.5f;
        public static readonly float CellDiagonalWidht = Mathf.Sqrt(CellWidht * 2f);
    }
}

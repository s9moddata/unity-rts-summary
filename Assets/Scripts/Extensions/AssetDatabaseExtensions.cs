﻿#if UNITY_EDITOR

namespace Assets.Scripts.Extensions
{
    using UnityEditor;
    using UnityEngine;

    public static class AssetDatabaseExtensions
    {
        public static void ForceSaveAsset(Object asset)
        {
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssetIfDirty(asset);
        }
    }
}

#endif

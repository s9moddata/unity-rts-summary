﻿namespace Assets.Scripts.Player
{
    using System.Collections.Generic;
    using Assets.Scripts.Pathfinder;
    using Assets.Scripts.Player.UnitSelection;
    using Assets.Scripts.Units;
    using UnityEngine;

    internal class Player : MonoBehaviour
    {
        [Header("Camera")]
        [SerializeField]
        private Camera _playerCamera;

        [Header("Terrain")]
        [SerializeField]
        private TerrainCollider _terrainCollider;

        [SerializeField]
        private Terrain _terrain;

        [Header("Scripts")]
        [SerializeField]
        private SelectionLogic _selectionLogic;

        private void Update()
        {
            UpdateInput();
        }

        private void UpdateInput()
        {
            if (PlayerInput.RightMouseButtonDown && _selectionLogic.HasSelectedUnits)
            {
                MoveUnits(_selectionLogic.SelectedUnits);
            }
        }

        private void MoveUnits(IReadOnlyList<BaseUnit> selectedUnits)
        {
            Ray ray = _playerCamera.ScreenPointToRay(Input.mousePosition);
            bool wasHit = _terrainCollider.Raycast(ray, out RaycastHit hit, 500f);

            if (wasHit)
            {
                foreach (BaseUnit unit in selectedUnits)
                {
                    IReadOnlyList<Vector2> path = PathfinderManager.GetPath(unit.Position, hit.point, unit.Clearance);
                    unit.FollowPath(path);
                }
            }
        }
    }
}

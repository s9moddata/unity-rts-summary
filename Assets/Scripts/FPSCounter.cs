﻿namespace Assets.Scripts
{
    using UnityEngine;

    public class FPSCounter : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField]
        private float _updateInterval = 1f;

        private float _accum = 0f;
        private float _timeleft = 0f;
        private float _fps = 0f;
        private int _frames = 0;

        private GUIStyle _textStyle = new GUIStyle();

        private void Start()
        {
            _timeleft = _updateInterval;

            _textStyle.fontStyle = FontStyle.Bold;
            _textStyle.normal.textColor = Color.white;
        }

        private void Update()
        {
            _timeleft -= Time.deltaTime;
            _accum += Time.timeScale / Time.deltaTime;
            _frames++;

            if (_timeleft <= 0f)
            {
                _timeleft = _updateInterval;

                _fps = _accum / _frames;
                _accum = 0f;
                _frames = 0;
            }
        }

        private void OnGUI()
        {
            Rect position = new Rect(5, 5, 100, 25);
            string text = _fps.ToString("F2") + "FPS";

            GUI.Label(position, text, _textStyle);
        }
    }
}

﻿namespace Assets.Scripts.Extensions
{
    using UnityEngine;

    public static class Vector3Extensions
    {
        public static Vector3 RotateAroundPivot(this Vector3 point, Vector3 pivot, Quaternion rotation)
        {
            Vector3 direction = point - pivot;
            return rotation * direction + pivot;
        }

        public static Vector2 ToVector2ZY(this Vector3 point) => new Vector2(point.x, point.z);

        public static Vector2Int RoundToVector2IntZY(this Vector3 point)
        {
            int pointX = Mathf.RoundToInt(point.x);
            int pointZ = Mathf.RoundToInt(point.z);
            return new Vector2Int(pointX, pointZ);
        }

        public static Vector3 LerpByDistance(Vector3 p0, Vector3 p1, float distance)
        {
            const float maxPercentage = 100f;

            float pointsDistance = Vector3.Distance(p0, p1);

            if (distance >= pointsDistance)
            {
                return p1;
            }

            float onePercent = pointsDistance / maxPercentage;
            float scaling = distance / onePercent / maxPercentage;

            return Vector3.Lerp(p0, p1, scaling);
        }
    }
}

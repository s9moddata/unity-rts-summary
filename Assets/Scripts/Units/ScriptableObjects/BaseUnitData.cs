﻿namespace Assets.Scripts.Units.ScriptableObjects
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "UnitData", menuName = "ScriptableObjects/Units/UnitData")]
    public class BaseUnitData : ScriptableObject
    {
        [Header("Speed Values")]
        [SerializeField]
        private float _rotationSpeed;

        [SerializeField]
        private float _movementSpeed;

        [Header("Clearance")]
        [SerializeField]
        private int _clearance;

        public float RotationSpeed => _rotationSpeed;

        public float MovementSpeed => _movementSpeed;

        public int Clearance => _clearance;
    }
}

﻿namespace Assets.Scripts.Extensions
{
    using UnityEngine;

    public static class Vector2Extensions
    {
        public static Vector2 LerpByDistance(Vector2 p0, Vector2 p1, float distance)
        {
            const float maxPercentage = 100f;

            float pointsDistance = Vector2.Distance(p0, p1);

            if (distance >= pointsDistance)
            {
                return p1;
            }

            float onePercent = pointsDistance / maxPercentage;
            float scaling = distance / onePercent / maxPercentage;

            return Vector2.Lerp(p0, p1, scaling);
        }

        /// <returns>Returns the angle between vectors whose direction intersects at a connection point.</returns>
        public static float Angle(Vector2 p0, Vector2 p1, Vector2 connectionPoint)
        {
            Vector2 direction0 = (connectionPoint - p0).normalized;
            Vector2 direction1 = (connectionPoint - p1).normalized;
            return Vector2.Angle(direction0, direction1);
        }
    }
}

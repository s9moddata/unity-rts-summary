﻿namespace Assets.Scripts.Pathfinder.Grid
{
    using System;
    using UnityEngine;
    using ClearanceConstants = Constants.Clearance;

    [Serializable]
    public struct Cell : IEquatable<Cell>
    {
        [SerializeField]
        private Vector2Int _worldPosition;

        [SerializeField]
        private int _chunkIndexX;

        [SerializeField]
        private int _chunkIndexY;

        [SerializeField]
        private int _clearance;

        [SerializeField]
        private bool _isObstacle;

        /// <summary>
        /// Creates an instance of <see cref="Cell"/> class.
        /// </summary>
        /// <param name="chunkIndexX">X position in chunk.</param>
        /// <param name="chunkIndexY">Y position in chunk.</param>
        public Cell(
            Vector2Int worldPosition,
            int chunkIndexX,
            int chunkIndexY,
            int clearance = ClearanceConstants.ClearanceMin,
            bool isObstacle = false)
        {
            _worldPosition = worldPosition;
            _chunkIndexX = chunkIndexX;
            _chunkIndexY = chunkIndexY;
            _clearance = clearance;
            _isObstacle = isObstacle;
        }

        /// <summary>
        /// Returns cell world position.
        /// </summary>
        public Vector2Int WorldPosition => _worldPosition;

        /// <summary>
        /// Returns the chunk X position.
        /// </summary>
        public int ChunkIndexX => _chunkIndexX;

        /// <summary>
        /// Returns the chunk Y position.
        /// </summary>
        public int ChunkIndexY => _chunkIndexY;

        public int Clearance => _clearance;

        public bool IsObstacle => _isObstacle;

        public static Cell Copy(Cell itemToCopy, int newClearance) => new Cell(
            itemToCopy.WorldPosition,
            itemToCopy.ChunkIndexX,
            itemToCopy.ChunkIndexY,
            newClearance,
            itemToCopy.IsObstacle);

        public static Cell Copy(Cell itemToCopy, bool newIsObstacle) => new Cell(
            itemToCopy.WorldPosition,
            itemToCopy.ChunkIndexX,
            itemToCopy.ChunkIndexY,
            itemToCopy.Clearance,
            newIsObstacle);

        public bool Equals(Cell other) => WorldPosition == other.WorldPosition;

        public override bool Equals(object obj)
        {
            bool isCell = obj is Cell;

            if (!isCell)
            {
                return false;
            }

            Cell other = (Cell)obj;
            return Equals(other);
        }

        public override int GetHashCode() => WorldPosition.GetHashCode();
    }
}

﻿namespace Assets.Editor.Windows
{
    using System.Collections.Generic;
    using System.IO;
    using Assets.Scripts;
    using UnityEditor;
    using UnityEngine;

    public class BoundingBoxGenerator : EditorWindow
    {
        private const string FolderName = "BoundingBoxes";

        private GameMapObject _gameMapObjectPrefab = null;
        private string _fileName = string.Empty;

        [MenuItem("WW2/Bounding Box/Generate")]
        private static void CreateWindow()
        {
            EditorWindow window = GetWindow(typeof(BoundingBoxGenerator));
            window.titleContent = new GUIContent("Generate Bounding Box");
            window.minSize = new Vector2(300f, 400f);
            window.position = new Rect(150f, 100f, 300f, 400f);
        }

        private void OnGUI()
        {
            _gameMapObjectPrefab = EditorGUILayout.ObjectField("Prefab", _gameMapObjectPrefab, typeof(GameMapObject), false) as GameMapObject;
            _fileName = EditorGUILayout.TextField("File Name", _fileName);

            EditorGUILayout.Space();

            if (GUILayout.Button("Create"))
            {
                ValidateDataFolderPath();
                CreateScriptableObject();
            }
        }

        private void CreateScriptableObject()
        {
            if (_gameMapObjectPrefab == null)
            {
                return;
            }

            // Instantiate object because you can't read correct Collider values of a prefab or an inactive object
            GameMapObject gameMapObject = Instantiate(_gameMapObjectPrefab); 

            List<Vector2Int> vertices = gameMapObject.GetInitialMeshVertices();
            BoundingBox data = BoundingBox.CreateInstance(vertices);

            // Use .gameObject to destroy the object, otherwise the object will not be destroyed
            DestroyImmediate(gameMapObject.gameObject);

            string fileName = string.IsNullOrWhiteSpace(_fileName) ? _gameMapObjectPrefab.name : _fileName;
            AssetDatabase.CreateAsset(data, $"Assets/{FolderName}/{fileName}.asset");
        }

        private void ValidateDataFolderPath()
        {
            string pathToCheck = $"{Application.dataPath}/{FolderName}";

            if (!Directory.Exists(pathToCheck))
            {
                Directory.CreateDirectory(pathToCheck);
            }
        }
    }
}

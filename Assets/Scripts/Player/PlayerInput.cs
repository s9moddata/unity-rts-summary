﻿namespace Assets.Scripts.Player
{
    using UnityEngine;

    public static class PlayerInput
    {
        private const int LeftMouseButtonId = 0;
        private const int RightMouseButtonId = 1;

        public static bool RightMouseButtonDown => Input.GetMouseButtonDown(RightMouseButtonId);

        public static bool LeftMouseButtonDown => Input.GetMouseButtonDown(LeftMouseButtonId);

        public static bool LeftMouseButton => Input.GetMouseButton(LeftMouseButtonId);

        public static bool LeftMouseButtonUp => Input.GetMouseButtonUp(LeftMouseButtonId);

        public static bool AddUnitsToSelectionKey => Input.GetKey(KeyCode.LeftShift);
    }
}

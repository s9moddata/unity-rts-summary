﻿namespace Assets.Scripts.CustomTypes
{
    using System.Collections.Generic;

    public class ReadOnlyIndexedList<T>
    {
        private const int FirstIndex = 0;
        private const int NoElements = 0;

        private readonly IReadOnlyList<T> List;
        private readonly int Count;

        private int _currentIndex = FirstIndex;

        public ReadOnlyIndexedList()
        {
            List = new List<T>();
            Count = NoElements;
        }

        public ReadOnlyIndexedList(IReadOnlyList<T> list)
        {
            List = list;
            Count = list.Count;
        }

        public T Value => List[_currentIndex];

        public bool IsEmptyOrOutOfBounds => _currentIndex >= Count;

        public bool TryGetValue(out T value, int indexOffset = 0)
        {
            int index = _currentIndex + indexOffset;

            if (index >= Count)
            {
                value = default(T);
                return false;
            }

            value = List[index];
            return true;
        }

        /// <summary>
        /// Increase current index value.
        /// </summary>
        /// <returns>Returns <see langword="true"/> if the current is out of <see cref="List"/> bounds.</returns>
        public bool Increase()
        {
            _currentIndex++;
            return IsEmptyOrOutOfBounds;
        }
    }
}

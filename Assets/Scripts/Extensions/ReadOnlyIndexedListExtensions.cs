﻿namespace Assets.Scripts.Extensions
{
    using Assets.Scripts.CustomTypes;

    public static class ReadOnlyIndexedListExtensions
    {
        /// <returns>Returns <see langword="true"/> if <paramref name="list"/> is <see langword="null"/>, empty or its current index is out of bounds.</returns>
        public static bool IsInvalid<T>(this ReadOnlyIndexedList<T> list) => list == null || list.IsEmptyOrOutOfBounds;
    }
}

﻿namespace Assets.Scripts.CustomTypes.Serializable
{
    using System;
    using UnityEngine;

    public struct SerializableGuid : ISerializationCallbackReceiver
    {
        [SerializeField]
        private byte[] _serializedGuid;

        public SerializableGuid(Guid guid)
        {
            Guid = guid;
            _serializedGuid = null;
        }

        public Guid Guid { get; private set; }

        public void OnBeforeSerialize()
        {
            _serializedGuid = Guid.ToByteArray();
        }

        public void OnAfterDeserialize()
        {
            Guid = new Guid(_serializedGuid);
        }
    }
}

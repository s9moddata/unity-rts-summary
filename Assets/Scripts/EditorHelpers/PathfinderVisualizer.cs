﻿#if UNITY_EDITOR

namespace Assets.Scripts.EditorHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Assets.Scripts.CustomTypes;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.AStar;
    using Assets.Scripts.Pathfinder.Grid;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEngine;

    public class PathfinderVisualizer : MonoBehaviour
    {
        [Header("Cached Window Values")]
        public bool _showWindow = true;

        public Vector2Int _startPosition = Vector2Int.zero;

        public Vector2Int _targetPosition = Vector2Int.zero;

        public int _agentClearance = 1;

        public int _initialCapacity = 0;

        public Color _lineColor = Color.black;

        public float _lineHeight = 0f;

        public GameMapSolid _gameMapSolid;

        [Header("Window Settings")]
        [SerializeField]
        private Vector2 _position = new Vector2(10f, 10f);

        [SerializeField]
        private float _width = 200f;

        [SerializeField]
        private float _height = 400f;

        private IReadOnlyList<Vector2> _pathfinderResult = new List<Vector2>();

        private string _pathfinderResultTime = string.Empty;

        public Vector2 Position => _position;

        public float Width => _width;

        public float Height => _height;

        public string PathfinderResultTime => _pathfinderResultTime;

        public void PerformPathfinding()
        {
            if (_gameMapSolid == null || _gameMapSolid.Cells == null)
            {
                return;
            }

            PathfinderAStar pathfinderAStar = new PathfinderAStar(new ReadOnlyArray2D<Cell>(_gameMapSolid.Cells), _initialCapacity);

            Stopwatch timer = new Stopwatch();
            timer.Start();

            bool isPathFound = pathfinderAStar.Calculate(
                _startPosition,
                _targetPosition,
                _agentClearance,
                out _pathfinderResult);

            timer.Stop();

            TimeSpan ts = timer.Elapsed;
            _pathfinderResultTime = isPathFound
                ? string.Format("{0:00}:{1:00}.{2:0000}", ts.Minutes, ts.Seconds, ts.Milliseconds)
                : "Path is not found!";
        }

        private void OnDrawGizmos()
        {
            DrawPath();
        }

        private void DrawPath()
        {
            if (!_showWindow || _pathfinderResult.IsNullOrEmpty())
            {
                return;
            }

            Vector2 first = _pathfinderResult.First();
            Vector3 previousPosition = new Vector3(first.x, _lineHeight, first.y);

            Gizmos.color = _lineColor;

            foreach (Vector2 position in _pathfinderResult)
            {
                Vector3 to = new Vector3(position.x, _lineHeight, position.y);

                Gizmos.DrawLine(previousPosition, to);

                previousPosition = to;
            }
        }
    }
}

#endif

﻿namespace Assets.Editor
{
    using Assets.Scripts.EditorHelpers;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(PathfinderVisualizer))]
    public class PathfinderVisualizerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }

        private void OnSceneGUI()
        {
            CreateUI();
        }

        private void CreateUI()
        {
            PathfinderVisualizer pathfinderVisualizer = target as PathfinderVisualizer;
            string showButtonText = pathfinderVisualizer._showWindow ? "Disable Visualizer" : "Activate Visualizer";
            Rect windowRect = new Rect(
                pathfinderVisualizer.Position.x,
                pathfinderVisualizer.Position.y,
                pathfinderVisualizer.Width,
                pathfinderVisualizer.Height);

            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            Handles.BeginGUI();
            GUILayout.BeginArea(windowRect);

            if (GUILayout.Button(showButtonText))
            {
                pathfinderVisualizer._showWindow = !pathfinderVisualizer._showWindow;
            }

            if (pathfinderVisualizer._showWindow)
            {
                GUILayout.BeginHorizontal();
                GUILayout.TextField("Map Data");
                pathfinderVisualizer._gameMapSolid = EditorGUILayout.ObjectField(pathfinderVisualizer._gameMapSolid, typeof(GameMapSolid), true) as GameMapSolid;
                GUILayout.EndHorizontal();

                pathfinderVisualizer._startPosition = EditorGUILayout.Vector2IntField("Start Position", pathfinderVisualizer._startPosition);
                pathfinderVisualizer._targetPosition = EditorGUILayout.Vector2IntField("Target Position", pathfinderVisualizer._targetPosition);
                pathfinderVisualizer._agentClearance = EditorGUILayout.IntField("Agent Clearance", pathfinderVisualizer._agentClearance);
                pathfinderVisualizer._initialCapacity = EditorGUILayout.IntField("Initial Capacity", pathfinderVisualizer._initialCapacity);
                pathfinderVisualizer._lineColor = EditorGUILayout.ColorField("Line Color", pathfinderVisualizer._lineColor);
                pathfinderVisualizer._lineHeight = EditorGUILayout.FloatField("Line Height", pathfinderVisualizer._lineHeight);

                if (GUILayout.Button("Perform Pathfinding"))
                {
                    pathfinderVisualizer.PerformPathfinding();
                }

                GUILayout.BeginHorizontal();
                GUILayout.TextField($"Alg. work time: {pathfinderVisualizer.PathfinderResultTime}");
                GUILayout.EndHorizontal();
            }

            GUILayout.EndArea();
            Handles.EndGUI();
        }
    }
}

﻿namespace Assets.Scripts.Pathfinder
{
    using System.Collections.Generic;
    using Assets.Scripts.CustomTypes;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.AStar;
    using Assets.Scripts.Pathfinder.Grid;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEngine;

    public class PathfinderManager : MonoBehaviour
    {
        [Header("Map Data")]
        [SerializeField]
        private GameMapSolid _gameMapSolid;

        private static PathfinderAStar _pathfinder;

        public static IReadOnlyList<Vector2> GetPath(Vector3 startPosition, Vector3 targetPosition, int agentClearance)
        {
            _pathfinder.Calculate(
                startPosition.RoundToVector2IntZY(),
                targetPosition.RoundToVector2IntZY(),
                agentClearance,
                out IReadOnlyList<Vector2> path);

            return path;
        }

        private void Awake()
        {
            _pathfinder = new PathfinderAStar(new ReadOnlyArray2D<Cell>(_gameMapSolid.Cells));
        }
    }
}

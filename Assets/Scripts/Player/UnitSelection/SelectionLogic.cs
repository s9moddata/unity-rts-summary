﻿namespace Assets.Scripts.Player.UnitSelection
{
    using System.Collections.Generic;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Units;
    using Assets.Scripts.World;
    using UnityEngine;

    [RequireComponent(typeof(MeshCollider))]
    internal class SelectionLogic : MonoBehaviour
    {
        private const float ColliderEnabledTime = 0.005f;
        private const float NoTime = 0f;
        private const int BoxCornerCount = 4;

        [Header("Scripts")]
        [SerializeField]
        private WorldUnitsList _worldUnitsList;

        [Header("Camera")]
        [SerializeField]
        private Camera _playerCamera;

        [SerializeField]
        private float _raycastDistance = 50f;

        [Header("Physics")]
        [SerializeField]
        private MeshCollider _meshCollider;

        [SerializeField]
        private LayerMask _unitLayerMask;

        private List<BaseUnit> _selectedUnits;
        private float _colliderEnabledTime;
        private Vector3 _fixedMousePosition;
        private bool _isDragSelect;

        public bool HasSelectedUnits => !_selectedUnits.IsNullOrEmpty();

        public IReadOnlyList<BaseUnit> SelectedUnits => _selectedUnits;

        private void Start()
        {
            _selectedUnits = new List<BaseUnit>();
        }

        private void Update()
        {
            UpdateColliderState();
            UpdatePlayerInput();
        }

        private void OnGUI()
        {
            if (_isDragSelect)
            {
                var rect = Utils.GetScreenRect(_fixedMousePosition, Input.mousePosition);
                Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
                Utils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            SelectUnit(other);
        }

        private void UpdateColliderState()
        {
            if (_colliderEnabledTime > NoTime)
            {
                _colliderEnabledTime -= Time.deltaTime;
            }
            else if (_meshCollider.enabled)
            {
                DisableCollider();
            }
        }

        private void UpdatePlayerInput()
        {
            const float dragSelectMagnitude = 40f;

            if (PlayerInput.LeftMouseButtonDown)
            {
                _fixedMousePosition = Input.mousePosition;
            }

            if (PlayerInput.LeftMouseButton)
            {
                float mousePositionMagnitude = (_fixedMousePosition - Input.mousePosition).magnitude;
                _isDragSelect = mousePositionMagnitude > dragSelectMagnitude;
            }

            if (PlayerInput.LeftMouseButtonUp)
            {
                if (!PlayerInput.AddUnitsToSelectionKey)
                {
                    DeselectAll();
                }

                if (_isDragSelect)
                {
                    FrameSelection();
                }
                else
                {
                    ClickSelection();
                }

                _isDragSelect = false;
            }
        }

        private void FrameSelection()
        {
            Vector3[] meshVertices = new Vector3[BoxCornerCount * 2];
            Vector2[] boundingBox = GetBoundingBox(_fixedMousePosition, Input.mousePosition);

            for (int i = 0; i < BoxCornerCount; i++)
            {
                Ray boundingBoxRay = _playerCamera.ScreenPointToRay(boundingBox[i]);
                meshVertices[i] = boundingBoxRay.origin;
                meshVertices[i + BoxCornerCount] = boundingBoxRay.GetPoint(_raycastDistance);
            }

            Mesh selectionMesh = GetSelectionMesh(meshVertices);
            EnableCollider(selectionMesh);
        }

        private void ClickSelection()
        {
            Ray ray = _playerCamera.ScreenPointToRay(_fixedMousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, _raycastDistance, _unitLayerMask))
            {
                SelectUnit(hit.collider);
            }
        }

        private void EnableCollider(Mesh colliderMesh)
        {
            _meshCollider.enabled = true;
            _meshCollider.sharedMesh = colliderMesh;
            _colliderEnabledTime = ColliderEnabledTime;
        }

        private void DisableCollider()
        {
            _meshCollider.enabled = false;
            _colliderEnabledTime = NoTime;
        }

        private Vector2[] GetBoundingBox(Vector2 corner1, Vector2 corner2)
        {
            Vector2 topRight = Vector2.Max(corner1, corner2);
            Vector2 bottomLeft = Vector2.Min(corner1, corner2);

            return new Vector2[BoxCornerCount]
            {
                new Vector2(bottomLeft.x, bottomLeft.y),
                new Vector2(topRight.x, bottomLeft.y),
                new Vector2(topRight.x, topRight.y),
                new Vector2(bottomLeft.x, topRight.y),
            };
        }

        private Mesh GetSelectionMesh(Vector3[] boxMeshVertices)
        {
            int[] boxMeshTriangles = {
                0, 2, 1, // front
			    0, 3, 2,
                2, 3, 4, // top
			    2, 4, 5,
                1, 2, 5, // right
			    1, 5, 6,
                0, 7, 4, // left
			    0, 4, 3,
                5, 4, 7, // back
			    5, 7, 6,
                0, 6, 7, // bottom
			    0, 1, 6
            };

            return new Mesh
            {
                vertices = boxMeshVertices,
                triangles = boxMeshTriangles
            };
        }

        private void SelectUnit(Collider unitCollider)
        {
            BaseUnit unit = _worldUnitsList.GetUnit(unitCollider);

            if (!_selectedUnits.Contains(unit))
            {
                _selectedUnits.Add(unit);
                unit.SetSelectionMarketActive(true);
            }
        }

        private void DeselectAll()
        {
            foreach (BaseUnit unit in _selectedUnits)
            {
                unit.SetSelectionMarketActive(false);
            }

            _selectedUnits = new List<BaseUnit>(); 
        }
    }
}

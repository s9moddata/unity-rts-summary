﻿#if UNITY_EDITOR

namespace Assets.Scripts.EditorHelpers
{
    using System.Collections.Generic;
    using Assets.Scripts.Extensions;
    using Assets.Scripts.Pathfinder.Grid;
    using Assets.Scripts.Pathfinder.ScriptableObjects;
    using UnityEditor;
    using UnityEngine;

    public class GameMapUpdater : MonoBehaviour
    {
        [Header("Cached Values")]
        public bool _showWindow = true;

        public bool _showGrid = false;

        public bool _showClearance = false;

        public bool _useSolidMap = true;

        public int _gridBrushSize;

        public TerrainCollider _terrainCollider;

        public GameMapSolid _gameMapSolid;

        public GameMap _gameMap;

        [HideInInspector]
        public HashSet<Vector2Int> _gridBrush = new HashSet<Vector2Int>();

        [Header("Window Settings")]
        [SerializeField]
        private Vector2 _windowPosition = new Vector2(10f, 10f);

        [SerializeField]
        private float _windowWidth = 200f;

        [SerializeField]
        private float _windowHeight = 400f;

        [Header("Key Settings")]
        [SerializeField]
        private KeyCode _placeObstacleKey = KeyCode.KeypadPlus;

        [SerializeField]
        private KeyCode _removeObstacleKey = KeyCode.KeypadMinus;

        public Rect WindowRect => new Rect(_windowPosition.x, _windowPosition.y, _windowWidth, _windowHeight);

        public KeyCode PlaceObstacleKey => _placeObstacleKey;

        public KeyCode RemoveObstacleKey => _removeObstacleKey;

        public bool HasSolidMapData => _showWindow && _terrainCollider != null && _gameMapSolid != null;

        public bool HasMapData => _showWindow && _gameMap != null;

        public void UpdateAndSaveGameMap()
        {
            SetObjectObstacles();

            _gameMapSolid.GenerateClearance();
            AssetDatabaseExtensions.ForceSaveAsset(_gameMapSolid);
        }

        public void SetObstacleStatus(IReadOnlyCollection<Vector2Int> coordinates, bool isObstacle)
        {
            if (coordinates.IsNullOrEmpty())
            {
                return;
            }

            foreach (Vector2Int gridPosition in coordinates)
            {
                _gameMapSolid.SetObstacleStatus(gridPosition, isObstacle);
            }
        }

        private void SetObjectObstacles()
        {
            GameMapObject[] gameMapObjects = FindObjectsOfType<GameMapObject>();

            foreach (GameMapObject gameMapObject in gameMapObjects)
            {
                IReadOnlyCollection<Vector2Int> verts = gameMapObject.GetMeshVertices();
                SetObstacleStatus(verts, true);
            }
        }

        private void OnDrawGizmos()
        {
            bool hasData = _useSolidMap ? HasSolidMapData : HasMapData;

            if (hasData)
            {
                if (_useSolidMap)
                {
                    DrawSolidGrid();
                    DrawSolidClearanceValues();
                }
                else
                {
                    DrawGrid();
                    DrawClearanceValues();
                }
            }
        }

        private void DrawGrid()
        {
            if (!_showGrid)
            {
                return;
            }

            foreach (Chunk chunk in _gameMap.Chunks)
            {
                foreach (Cell cell in chunk.ChuckCells)
                {
                    DrawCell(cell, chunk.ChunkDebugColor);
                }
            }
        }

        private void DrawSolidGrid()
        {
            if (!_showGrid)
            {
                return;
            }

            foreach (Cell cell in _gameMapSolid.Cells)
            {
                DrawCell(cell, Color.blue);
            }
        }

        private void DrawCell(Cell cell, Color freeTileColor)
        {
            const float gridHeight = 0f;

            Vector3 spawnCoordinate = new Vector3(cell.WorldPosition.x, gridHeight, cell.WorldPosition.y);

            Gizmos.color = cell.IsObstacle ? Color.red : freeTileColor;

            if (cell.IsObstacle)
            {
                Gizmos.DrawCube(spawnCoordinate, Vector3.one);
            }

            if (!_gridBrush.IsNullOrEmpty() && _gridBrush.Contains(cell.WorldPosition))
            {
                Gizmos.color = Color.green;
                Gizmos.DrawCube(spawnCoordinate, Vector3.one);
            }
            else
            {
                Gizmos.DrawWireCube(spawnCoordinate, Vector3.one);
            }
        }

        //private void DrawBoundingBoxes()
        //{
        //    if (!_showBoundingBoxes)
        //    {
        //        return;
        //    }

        //    MeshCollider meshCollider = _tempoObj.GetComponent<MeshCollider>();

        //    if (meshCollider == null)
        //    {
        //        return;
        //    }

        //    Gizmos.color = Color.green;
        //    Gizmos.DrawWireCube(meshCollider.bounds.center, meshCollider.bounds.size);
        //}

        private void DrawClearanceValues()
        {
            if (!_showClearance)
            {
                return;
            }

            foreach (Chunk chunk in _gameMap.Chunks)
            {
                foreach (Cell cell in chunk.ChuckCells)
                {
                    DrawClearanceValue(cell);
                }
            }
        }

        private void DrawSolidClearanceValues()
        {
            if (!_showClearance)
            {
                return;
            }

            foreach (Cell cell in _gameMapSolid.Cells)
            {
                DrawClearanceValue(cell);
            }
        }

        private void DrawClearanceValue(Cell cell)
        {
            const float valueHeight = 0.5f;

            if (cell.IsObstacle)
            {
                return;
            }

            Vector3 spawnCoordinate = new Vector3(cell.WorldPosition.x, valueHeight, cell.WorldPosition.y);

            GUI.color = Color.black;
            Handles.Label(spawnCoordinate, cell.Clearance.ToString());
        }
    }
}

#endif
